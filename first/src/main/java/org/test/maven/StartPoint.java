package org.test.maven;

public class StartPoint {
    public static void main(String[] args) {
        System.out.println("Starting-->>");
        if (args.length > 0) {
            for (String arg : args) {
                System.out.print(" " + arg);
            }
        }
        System.out.println("\n<<-- Finish");

    }
}
